pub fn calculate_average(values: &[f64]) -> f64 {
    if values.is_empty() {
        return 0.0;
    }
    let sum: f64 = values.iter().sum();
    sum / values.len() as f64
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_average() {
        let values = vec![10.0, 20.0, 30.0];
        let average = calculate_average(&values);
        assert_eq!(average, 20.0);
    }

    #[test]
    fn test_calculate_average_empty() {
        let values: Vec<f64> = vec![];
        let average = calculate_average(&values);
        assert_eq!(average, 0.0);
    }
}
