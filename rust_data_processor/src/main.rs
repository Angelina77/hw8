use clap::{App, Arg};
use csv::ReaderBuilder;
use serde::Deserialize;
use std::error::Error;

#[derive(Debug, Deserialize)]
struct Record {
    id: String,
    value: f64,
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("rust_data_processor")
        .version("1.0")
        .author("Your Name")
        .about("Processes CSV data")
        .arg(
            Arg::with_name("input")
                .short('i')
                .long("input")
                .value_name("FILE")
                .help("Sets the input CSV file to use")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let file_path = matches.value_of("input").unwrap();

    let mut rdr = ReaderBuilder::new().from_path(file_path)?;

    let mut total: f64 = 0.0;
    let mut count: usize = 0;
    for result in rdr.deserialize() {
        let record: Record = result?;
        total += record.value;
        count += 1;
    }

    let average = if count > 0 {
        total / count as f64
    } else {
        0.0
    };

    println!("Average value: {:.2}", average);

    Ok(())
}
