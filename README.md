# hw8

## Build the project using Cargo

To build the project, navigate to the project directory and run the following command:

```sh
cargo build
```

### rust main function
The Rust Data Processor is a command-line application that reads data from a CSV file and calculates the average value of a specified column. The application uses the `clap` crate to parse command-line arguments and the `csv` crate to read the CSV file.

code in src/main.rs:

```rust
fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("rust_data_processor")
        .version("1.0")
        .author("Your Name")
        .about("Processes CSV data")
        .arg(
            Arg::with_name("input")
                .short('i')
                .long("input")
                .value_name("FILE")
                .help("Sets the input CSV file to use")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let file_path = matches.value_of("input").unwrap();

    let mut rdr = ReaderBuilder::new().from_path(file_path)?;

    let mut total: f64 = 0.0;
    let mut count: usize = 0;
    for result in rdr.deserialize() {
        let record: Record = result?;
        total += record.value;
        count += 1;
    }

    let average = if count > 0 {
        total / count as f64
    } else {
        0.0
    };

    println!("Average value: {:.2}", average);

    Ok(())
}
```


Running the Application
To process a CSV file, run:

```sh
cargo run -- -i src/sample.csv
```

sample output:
![alt text](image-1.png)
We can see that the application reads the data from the CSV file and calculates the average value of the specified column which is 18.00 in this case.


This project includes unit tests to ensure the reliability and correctness of its key functions. To run these tests, use:

```sh
cargo test
```


### rust test
code in src/lib.rs
```rust
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_average() {
        let values = vec![10.0, 20.0, 30.0];
        let average = calculate_average(&values);
        assert_eq!(average, 20.0);
    }

    #[test]
    fn test_calculate_average_empty() {
        let values: Vec<f64> = vec![];
        let average = calculate_average(&values);
        assert_eq!(average, 0.0);
    }
}
```

### test results
![alt text](image.png)




